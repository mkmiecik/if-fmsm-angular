if-fmsm: angular
===============

Jak wystartować?
----------------

Instalujemy NodeJS: http://nodejs.org/

Mając zainstalowanego Node'a piszemy w konsoli:
```
npm install -g gulp
npm install -g bower
npm install -g http-server
```
Flaga `-g` instaluje nam globalnie komendy (`gulp`, `bower`), które możemy następnie używać w systemie.

Następnie w katalogu głównym (tam gdzie jest `bower.json` oraz `package.json`)

```
npm install
bower install
hs .
```

Komenda `npm install` ściąga nam odpowiednie pakiety node'a, `bower install` ściąga odpowiednie pakiety
potrzebne do front-endu. `hs .` startuje nam lokalny serwer udostępniający pliki w danym katalogu
na adresie: `localhost:8080`.

Gdyby pojawiał się błąd `Error: Invalid forward slash character` to trzeba wejść bezpośrednio
na `http://localhost:8080/index.html`.

