/**
 * Created by Mateusz on 2015-01-15.
 */

// tu definiowany jest główny moduł naszej aplikacji, w tablicy są podane zależności, inne moduły, które będziemy wykorzystywać
var app = angular.module('fmsm-app', [
    'ui.router'
]);


app.config(function ($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/actions/');

    $stateProvider
        .state('actions', {
            url: '/actions',
            abstract: true,
            template: '<ui-view></ui-view>'
        })
        .state('actions.list', {
            url: '/',
            templateUrl: 'views/actions/index.html',
            controller: 'homeController'
        })
        .state('actions.new', {
            url: '/new',
            templateUrl: 'views/actions/new.html'
        })
        .state('users', {
            url: '/users',
            abstract: true,
            template: '<ui-view></ui-view>'
        })
        .state('users.list', {
            url: '/',
            templateUrl: 'views/users/index.html'
        })
        .state('my-profile', {
            url: '/profile',
            templateUrl: 'views/my-profile.html'
        });

});

app.run(function(){

});